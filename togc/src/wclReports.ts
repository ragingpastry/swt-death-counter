import { AccessToken, ReportData, Env, EventData } from "./types";

async function getToken(env: Env) {
  const form = new FormData();
  form.append("grant_type", "client_credentials");

  const response = await fetch("https://www.warcraftlogs.com/oauth/token", {
    method: "POST",
    body: form,
    headers: {
      Authorization:
        "Basic " + btoa(env.CLIENT_ACCESS_KEY + ":" + env.CLIENT_SECRET_KEY),
    },
  });

  const token: AccessToken = await response.json();

  if (response.ok) {
    if (token.access_token) {
      return token.access_token;
    } else {
      return Promise.reject(new Error(`No access token was provided`));
    }
  } else {
    const error = new Error(`Error`);
    return Promise.reject(error);
  }
}

export async function getGuildReports(env: Env) {
  const token = await getToken(env);
  const query = `
    query {
      reportData {
        reports(
          guildID: ${env.GUILD_ID}
          startTime: 0
          zoneID: ${env.ZONE_ID}
        ) {
          total
          per_page
          data {
            code
            startTime
            endTime
            table(
              startTime: 0
              endTime: ${Math.floor(Date.now() / 1000)}
              dataType: Deaths
              hostilityType: Friendlies
            )
            title
            rankedCharacters {
              name
            }
            fights {
              id
              name
              kill
            }
          }
        }
      }
    }
  `;
  const response = await fetch(
    "https://classic.warcraftlogs.com/api/v2/client",
    {
      method: "POST",
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        query: query,
      }),
    }
  );

  type JSONResponse = {
    data?: {
      reportData: {
        reports: {
          data: Array<ReportData>;
        };
      };
    };
    errors?: Array<{ message: string }>;
  };

  const { data, errors }: JSONResponse = await response.json();
  if (response.ok) {
    if (errors) {
      return Promise.reject(new Error(errors.map((e) => e.message).join("\n")));
    }
    const reports = data?.reportData.reports.data;
    if (reports) {
      return reports;
    } else {
      return Promise.reject(
        new Error(`No reports found for guildTagID "${env.GUILD_ID}"`)
      );
    }
  } else {
    // handle the graphql errors
    const error = new Error(
      errors?.map((e) => e.message).join("\n") ?? "unknown"
    );
    return Promise.reject(error);
  }
}

export async function getReportEvents(env: Env, fights: Array<number>) {
  const token = await getToken(env);
  const query = `
    query {
      reportData {
        reports(
          guildID: ${env.GUILD_ID}
          startTime: 0
          zoneID: 1018
        ) {
          data {
            code
            startTime
            endTime
            events(
              killType: Encounters
              limit: 10000
              dataType: Casts
              abilityID: 1949
              fightIDs: ${JSON.stringify(fights)}
            ) {
              data
            }
          }
        }
      }
    }
  `;
  const response = await fetch(
    "https://classic.warcraftlogs.com/api/v2/client",
    {
      method: "POST",
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        query: query,
      }),
    }
  );

  type JSONResponse = {
    data?: {
      reportData: {
        reports: {
          data: Array<EventData>;
        };
      };
    };
    errors?: Array<{ message: string }>;
  };

  const { data, errors }: JSONResponse = await response.json();
  if (response.ok) {
    if (errors) {
      return Promise.reject(new Error(errors.map((e) => e.message).join("\n")));
    }
    const reports = data?.reportData.reports.data;
    if (reports) {
      return reports;
    } else {
      return Promise.reject(
        new Error(`No reports found for guildTagID "${env.GUILD_ID}"`)
      );
    }
  } else {
    // handle the graphql errors
    const error = new Error(
      errors?.map((e) => e.message).join("\n") ?? "unknown"
    );
    return Promise.reject(error);
  }
}
