export type AccessToken = {
  token_type: string;
  expires_in: number;
  access_token: string;
};

export type FightEntry = {
  id: number;
  name: string;
  kill: boolean;
};

export type CharacterEntry = {
  name: string;
};

export type ReportData = {
  code: string;
  startTime: number;
  endTime: number;
  title: string;
  fights: Array<FightEntry>;
  rankedCharacters: Array<CharacterEntry>;
  events?: Array<EventEntry>;
  table: {
    data: {
      entries: Array<Death>;
    };
  };
};

export type EventEntry = {
  timestamp: number;
  sourceID: number;
  fight: number;
  abilityGameID: number;
  type: string;
};

export type EventData = {
  code: string;
  startTime: number;
  endTime: number;
  events: {
    data: Array<EventEntry>;
  };
};

export interface Event {
  cron: string;
  type: string;
  scheduledTime: number;
}

export class Player {
  public name: string;
  public deaths: Array<Death>;

  constructor(name: string, death: Death) {
    this.name = name;
    this.deaths = [death];
  }

  static checkDeathFor(death: Death, killingBlowName: string) {
    if (death.killingBlow! && death.killingBlow!.name == killingBlowName) {
      return true;
    } else {
      return false;
    }
  }
}

export type DamageAbility = {
  name: string;
  total: number;
  totalReduced?: number;
  type: number;
};

export type HealingAbility = {
  name: string;
  total: number;
  type: number;
};

export type AbilitySource = {
  name: string;
  total: number;
  totalReduced?: number;
  type: string;
};

export type EventAbility = {
  name: string;
  guid: number;
  type: number;
  abilityIcon: string;
};

export type DeathEvents = {
  timestamp: number;
  type: string;
  sourceID: number;
  sourceIsFriendly: boolean;
  targetID: number;
  targetIsFriendly: boolean;
  ability: EventAbility;
  fight: number;
  hitType: number;
  amount: number;
  mitigated: number;
  unmitigatedAmount: number;
  overkill?: number;
  absorbed?: number;
  targetMarker?: number;
};

export interface Death {
  name: string;
  id: number;
  guid: number;
  type: string;
  icon: string;
  timestamp: number;
  fight: number;
  fightName?: string;
  damage: {
    total: number;
    totalReduced: number;
    activeTime: number;
    activeTimeReduced: number;
    overheal: number;
    abilities: Array<DamageAbility>;
    sources: Array<AbilitySource>;
  };
  healing: {
    total: number;
    totalReduced: number;
    activeTime: number;
    activeTimeReduced: number;
    abilities: Array<HealingAbility>;
    damageAbilities: Array<DamageAbility>;
    sources: Array<AbilitySource>;
  };
  deathWindow: number;
  overkill: number;
  events: Array<DeathEvents>;
  killingBlow?: {
    name: string;
    guid: number;
    type: number;
    abilityIcon: string;
  };
}

export const tankTypes = [
  "Blood",
  "Lichborne",
  "Guardian",
  "Protection",
  "Champion",
];

export type Cache = {
  data: Array<Player>;
};

export interface Env {
  SWT_DEATH_COUNTER: KVNamespace;
  GUILD_ID: number;
  CLIENT_ACCESS_KEY: string;
  CLIENT_SECRET_KEY: string;
  ZONE_ID: number;
}
