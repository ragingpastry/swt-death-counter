import {
  Player,
  Death,
  ReportData,
  EventEntry,
  Cache,
  Env,
  Event,
  tankTypes,
} from "./types";
import { getGuildReports, getReportEvents } from "./wclReports";
import { buildResponse } from "./response";

async function setDeaths(env: Env, data: string) {
  await env.SWT_DEATH_COUNTER.put("data", data);
}

async function getDeaths(env: Env) {
  const defaultData = { data: [new Player("unknown", {} as Death)] };
  let data: Cache;
  const cache = await env.SWT_DEATH_COUNTER.get("data");
  if (!cache) {
    await setDeaths(env, JSON.stringify(defaultData));
    data = defaultData;
  } else {
    data = JSON.parse(cache);
  }
  return data.data;
}

/**
 * Filter fights in a report for all deaths which happen after a "wipe" which is defined as all tanks being dead
 *  @param {ReportData} report A report to search through
 *  @return {Array<Death>} A list of deaths which have happened after a wipe
 */
async function filterWipes(report: ReportData) {
  const deathsAfterWipe: Array<Death> = [];

  for (const fight of report.fights.map((fight) => fight.id)) {
    const deathsByFights = report.table.data.entries.filter(
      (entry) => entry.fight == fight
    );
    const tankDeaths = deathsByFights.filter((entry) =>
      tankTypes.includes(entry.icon.split("-")[1])
    );
    const isFightAWipe = report.fights.find(
      (entry) => entry.id == fight && entry.kill != true
    );
    const thoriumTrash = report.fights.find(
      (entry)  => entry.id == fight && entry.name == "Thorim"
    )
    console.log(thoriumTrash)
    if (isFightAWipe) {
      const wipeCalledEvent = report.events?.find(
        (event) => event.fight == fight
      );
      if (wipeCalledEvent && wipeCalledEvent.timestamp) {
        deathsByFights
          .filter((entry) => entry.timestamp > wipeCalledEvent.timestamp)
          .map((e) => deathsAfterWipe.push(e));
      } else if (tankDeaths.length > 0) {
        const tankDeathTimestamp = Math.max(
          ...tankDeaths.map((e) => e.timestamp)
        );
        report.table.data.entries
          .filter(
            (entry) =>
              entry.fight == fight && entry.timestamp > tankDeathTimestamp
          )
          .map((e) => deathsAfterWipe.push(e));
      }
    }
  }

  return deathsAfterWipe;
}

async function parseReports(reports: Array<ReportData>) {
  const playerList: Array<Player> = [];
  for (const report of reports.filter(
    (report, index, self) =>
      // Filter out reports which are uploaded on the same day to remove potential duplicate reports
      index ===
        self.findIndex(
          (t) =>
            new Date(t.startTime).toLocaleDateString("en-US") ===
              new Date(report.startTime).toLocaleDateString("en-US") &&
            new Date(t.endTime).toLocaleDateString("en-US") ===
              new Date(report.endTime).toLocaleDateString("en-US")
        ) &&
      // Filter out 10 man reports
      report.rankedCharacters.length >= 25
  )) {
    const deathsAfterWipe = await filterWipes(report);
    // Filter out deaths which occured after a wipe
    for (const death of report.table.data.entries.filter(
      (x) => !deathsAfterWipe.includes(x)
    )) {
      const fightName = report.fights.find((fight) => fight.id == death.fight);
      if (fightName) {
        death.fightName = fightName.name;
      }
      // DI shouldn't count towards ape deaths
      if (Player.checkDeathFor(death, "Divine Intervention")) {
        continue;
      }

      if (!playerList.map((player) => player.name).includes(death.name)) {
        const player = new Player(death.name, death);
        playerList.push(player);
      } else {
        const player = playerList.find((player) => player.name == death.name);
        if (player != undefined) {
          player.deaths.push(death);
        }
      }
    }
  }
  playerList.sort((a, b) => a.deaths.length - b.deaths.length).reverse();
  return playerList;
}

async function updateDeaths(env: Env) {
  const reports = await getGuildReports(env);
  const fights = reports
    .map((entry) => entry.fights.map((fight) => fight.id))
    .flat();
  // We are forced to do this because a single query is too "complex" for WCL and gets
  // rejected. So instead we make one call to gather the death table, and another call
  // to gather any events of type 1949
  // Eventually this should be a variable that we pass via wrangler
  const events = await getReportEvents(env, fights);
  for (const report of reports) {
    const matchingReport = events.find((event) => event.code == report.code);
    if (matchingReport) {
      const matchingEvents: Array<EventEntry> = matchingReport?.events.data;
      report.events = matchingEvents;
    }
  }
  const deaths = await parseReports(reports);
  const data = {
    data: deaths,
  };
  await setDeaths(env, JSON.stringify(data));

  return deaths;
}

export default {
  async fetch(request: Request, env: Env) {
    if (request.method == "PUT") {
      const deaths = await updateDeaths(env);
      return buildResponse(deaths);
    } else {
      const deaths = await getDeaths(env);
      return buildResponse(deaths);
    }
  },

  async scheduled(event: Event, env: Env, ctx: ExecutionContext) {
    ctx.waitUntil(updateDeaths(env));
  },
};
