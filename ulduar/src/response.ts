import { Player, Death } from "./types";
export function buildResponse(players: Array<Player>, status = 200) {
  const html = `
    <!DOCTYPE html>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.2/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.3.4/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/4.2.1/css/fixedColumns.dataTables.min.css">

    <html>
    <script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.3.4/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.3.4/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/fixedcolumns/4.2.1/js/dataTables.fixedColumns.min.js"></script>


    <script type="text/javascript">
      $(document).ready( function () {
        let stupidDeathColumns = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        var table = $('#deathCounter').DataTable({
              dom: 'Bfrtip',
              paging: false,
              order: [[1, 'desc']],
              responsive: true,
              scrollY: '60vh',
              scrollX: true,
              scrollCollapse: false,
              fixedColumns: {
                left: 1
              },
              columnDefs: [
                { targets: stupidDeathColumns, className: "dt-body-center", visible: false},
                { targets: [1], className: "dt-body-center"},
              ],
              buttons: [
                {
                  text: 'Toggle Stupid Deaths',
                  action: function(e, dt, node, config) {
                    table.columns(stupidDeathColumns).visible(!this.active());
                    this.active(!this.active())
                  }
                },
                {
                  extend: 'colvis',
                  columns: ':not(.noVis)'
                }
              ]
        });
        $('.dataTables_scrollHead').css({
            'overflow-x':'scroll'
        }).on('scroll', function(e){
            var scrollBody = $(this).parent().find('.dataTables_scrollBody').get(0);
            scrollBody.scrollLeft = this.scrollLeft;
            $(scrollBody).trigger('scroll');
        });
      } );
    </script>

    <head>

        <title>Death Counter</title>
    <style>
      .header {
        text-align: center;
      }
      button.dt-button,div.dt-button,a.dt-button,input.dt-button {
        color: white;
      }
      div.dt-button-collection {
        background: linear-gradient(to bottom, rgba(80, 95, 102, 0.1) 0%, rgba(0, 0, 0, 0.1) 100%)
      }
      button.dt-button:active:not(.disabled), div.dt-button-collection button.dt-button.active:not(.disabled), div.dt-button-collection div.dt-button:active:not(.disabled), div.dt-button-collection div.dt-button.active:not(.disabled), div.dt-button-collection a.dt-button:active:not(.disabled), div.dt-button-collection a.dt-button.active:not(.disabled) {
        background: rgba(80,95,102,1)
      }
      th {
        white-space: nowrap;
      }
      table.dataTable tbody tr>.dtfc-fixed-left, table.dataTable tbody tr>.dtfc-fixed-right {
        background-color: rgba(31, 42, 54, 1);
      }
      table.dataTable thead tr>.dtfc-fixed-left, table.dataTable thead tr>.dtfc-fixed-right, table.dataTable tfoot tr>.dtfc-fixed-left, table.dataTable tfoot tr>.dtfc-fixed-right {
        background-color: rgba(31, 42, 54, 1);
      }
    </style>
      <link rel="icon" type="image/x-icon" href="https://pub-d2d8061189b74107abd60f594d7840f7.r2.dev/icons8-gorilla-50.png?v=2">
    </head>
    <body>
        <div class="header">
        <h1>TOGC Ape Rankings</h1>
        <h2>Top Ape: ${
          players.reduce((a, b) => (a.deaths.length > b.deaths.length ? a : b))
            .name
        }</h2>
        <img src="https://pub-d2d8061189b74107abd60f594d7840f7.r2.dev/ape_god.png">
        </div>
        <br>
        <table id="deathCounter" class="display" style="width:50%">
          <thead>
            <tr>
              <th>Player</th>
              <th>Deaths</th>
              <th>Cosmic Smash</th>
              <th>Rocket Strike</th>
              <th>Proximity Mine</th>
              <th>P3Wx2 Laser Barrage</th>
              <th>Biting Cold</th>
              <th>Shadow Crash</th>
              <th>Sun Beam</th>
              <th>Lightning Charge</th>
              <th>Thorim Trash</th>
              <th>Kologarn Falling</th>
            </tr>
          </thead>
          <tbody>
            ${players
              .map(
                (player: Player) => `
            <tr>
              <td>${player.name}</td>
              <td>${player.deaths.length}</td>
              <td>${
                player.deaths.filter((death: Death) =>
                  Player.checkDeathFor(death, "Cosmic Smash")
                ).length
              }</td>
              <td>${
                player.deaths.filter((death: Death) =>
                  Player.checkDeathFor(death, "Rocket Strike")
                ).length
              }</td>
              <td>${
                player.deaths.filter((death: Death) =>
                  Player.checkDeathFor(death, "Explosion")
                ).length
              }</td>
              <td>${
                player.deaths.filter((death: Death) =>
                  Player.checkDeathFor(death, "P3Wx2 Laser Barrage")
                ).length
              }</td>
              <td>${
                player.deaths.filter((death: Death) =>
                  Player.checkDeathFor(death, "Biting Cold")
                ).length
              }</td>
              <td>${
                player.deaths.filter((death: Death) =>
                  Player.checkDeathFor(death, "Shadow Crash")
                ).length
              }</td>
              <td>${
                player.deaths.filter((death: Death) =>
                  Player.checkDeathFor(death, "Unstable Energy")
                ).length
              }</td>
              <td>${
                player.deaths.filter((death: Death) =>
                  Player.checkDeathFor(death, "Lightning Charge")
                ).length
              }</td>
              <td>${
                player.deaths.filter((death: Death) =>
                  Player.checkDeathFor(death, "Whirling Trap")
                ).length
              }</td>
              <td>${
                player.deaths.filter(
                  (death: Death) =>
                    death.fightName == "Kologarn" &&
                    death.killingBlow == undefined
                ).length
              }
              </td>
            </tr>
            `
              )
              .join("")}
          </tbody>
        </table>
    </body>
    <footer>
      <p>Deaths are counted when they occur before a "wipe". A "wipe" is defined as the first timestamp at which all tanks are dead.</p>
    </html>
  `;

  return new Response(html, {
    status,
    headers: {
      // Content-Type must include text/html for live reload to work
      "Content-Type": "text/html; charset=UTF-8",
    },
  });
}
